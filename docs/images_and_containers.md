# Images and Containers

## Images

Images are immutalbe files containing a snapshot of a docker container. Images are built using the `build` command based on the instructions of a Dockerfile. As all dependencies of an application are installed during a build, the images can become quite large. Due to this, every instruction from the Dockerfile creates a new layer, which can be transfered with a minimal amount of data. Images aren't executed as it, but when you run an image, a container based on these image will be created.([^1])

In other words: 

!!! quote
    Images are just templates for docker containers.[^2]

!!! quote
    An image is based on another image, with some additional customization[^3]

### Pull

Image can be downloaded from an registry with the `pull` command:

```bash
docker pull alpine:latest
```

???+ tip "Minimal data transfer with layers"
    Pull the nginx:mainline-alpine image:
    ```bash
    docker pull nginx:mainline-alpine
    ```
    and you should see an output similar to this:
    ```bash
    mainline-alpine: Pulling from library/nginx
    89d9c30c1d48: Already exists 
    110ad692b782: Pull complete 
    Digest: sha256:085e84650dbe56f27ca3ed00063a12d5b486e40c3d16d83c4e6c2aad1e4045ab
    Status: Downloaded newer image for nginx:mainline-alpine
    docker.io/library/nginx:mainline-alpine
    ```

    Notice the line mentioning: **`Already exists`**. This means, that the underlying alpine image was already pulled before and didn't need to be downloaded again.

All local images can be listed with:
```bash
docker images
# or
docker image ls
```

???+ hint "Search images on a registry from the command line"
    You can search images stored on a registry from the command line with the `search` command:
    ```bash
    docker search python
    ```
    You will also see additional information like the stars or if it's an official image.

    ??? quote "Official Images on Docker Hub"
        The Docker Official Images are a curated set of Docker repositories hosted on Docker Hub. They are designed to:

        - Provide essential base OS repositories (for example, ubuntu, centos) that serve as the starting point for the majority of users.
        - Provide drop-in solutions for popular programming language runtimes, data stores, and other services, similar to what a Platform as a Service (PAAS) would offer.
        - Exemplify Dockerfile best practices and provide clear documentation to serve as a reference for other Dockerfile authors.
        - Ensure that security updates are applied in a timely manner. This is particularly important as many Official Images are some of the most popular on Docker Hub.

        From: Official Images on Docker Hub[^4]


Once you have pulled or built an image, you can create a container out of it.

## Containers

Containers are based on images. They are runnable instances of images, which you can start, stop, kill, move and so on. The containers are isolated from the host system and other containers, but the level of isolation can be customized. Containers are transient, this means, nothing will be persisted unless you configure additional persistence.

### Create
A docker container can be created from an image with the `create` command:
```bash
docker create nginx:mainline-alpine
```
and all existing containers can be listed with the following command:
```bash
docker ps -a
```
You should now see the just created nginx container.

### Start

To start you can use the `start` command:
First you need to know the name of the container you want to start. The names of a container is listed with the `docker ps -a` command. Then start the container:
```bash
docker start CONTAINER_NAME
```
And then you can run:
```bash
docker ps
```
and should will see the just started container.

!!! hint "docker ps -a vs. docker ps"
    `docker ps -a` lists every existing container, while `docker ps` only lists the currently running containers.


## Exercises

### Start a container of your favorite programming language

Start a container based on an image of your favorite programming language using the above described commands.

!!! attention "Started container is not listed"
    The container will most likely be started and stopped immediately. Due to that you can't check if the container is running with `docker ps`.
    Use `docker ps -a` instead and check the STATUS, if the container exited a few minutes ago.

??? success "Solution"
    The following solution is for ruby. But it should work the same way for your favorite language too.
    ```bash
    docker search ruby
    docker pull ruby
    docker create ruby
    docker ps -a # To get the containers name
    docker start CONTAINER_NAME
    ```
    To check if the container was running, it's status should be similar to this:
    ```bash
    $ docker ps -a
    CONTAINER ID  IMAGE  COMMAND  CREATED        STATUS                    PORTS  NAMES
    c4237e549a5e  ruby   "irb"    5 minutes ago  Exited (0) 3 minutes ago         trusting_meninsky
    ```



### The same but simpler
Which command simplifies the process from exercise one?
How can you run a docker container?

???+ hint
    It's one single command. Use your google skills to examine it.

??? success "Solution"
    The following solution is for ruby. But it should work the same way for your favorite language too.
    ```bash
    docker run ruby
    ```
    To check if the container was running, it's status should be similar to this:
    ```bash
    $ docker ps -a
    CONTAINER ID  IMAGE  COMMAND  CREATED        STATUS                    PORTS  NAMES
    c4237e549a5e  ruby   "irb"    5 minutes ago  Exited (0) 3 minutes ago         trusting_meninsky
    ```

[^1]: What's an Image?. Retrieved November 6, 2019, from [https://stackoverflow.com/a/26960888/7130107](https://stackoverflow.com/a/26960888/7130107).
[^2]: Docker Cheat Sheet. Retrieved November 6, 2019, from [https://github.com/wsargent/docker-cheat-sheet/blob/master/README.md#images](https://github.com/wsargent/docker-cheat-sheet/blob/master/README.md#images).
[^3]: Docker overview. Retrieved November 6, 2019, from [https://docs.docker.com/engine/docker-overview/](https://docs.docker.com/engine/docker-overview/).
[^4]: Official Images on Docker Hub. Retrieved November 6, 2019, from [https://docs.docker.com/docker-hub/official_images/](https://docs.docker.com/docker-hub/official_images/).
