# Run your first container
We've already ran some containers, but they exited immediately after the startup.

In this section you will learn how to serve a website you're currently developing using docker.

## Grab you a project
This should be a simple html/css/js project. If you don't have one, take the following: [gibm-timetable](https://github.com/dominikstraessle/gibm-timetable).

??? attention "Allow http in the firewall"
    Make sure you allow http in your firewall first:
    ```bash
    sudo ufw allow http
    ```
### nginx

!!! info "`-p` and `-v`"
    From now on you will see the `-p` and `-v` options very often. Read what they mean:

    **`-p`**  
    This is a port-binding. `-p 80:8181` will bind the port 80 from the host to the port 8181 from the container. This is required, because docker containers are in a different network than the host by default.[^1]

    **`-v`**  
    This is a volume-binding. `-v $(pwd):/usr/share/nginx/html` will bind the current working directory from the host to the `/usr/share/nginx/html` inside the container. This means, if you edit a file in the current working directory, it will also be edited in the container.[^2]

```bash
git clone https://github.com/dominikstraessle/gibm-timetable.git
cd gibm-timetable
docker run -p 80:80 -v $(pwd):/usr/share/nginx/html nginx:mainline-alpine
```
Now open the website in the browser. It's accessible on your ip adress.

Terminat the process by pressing Ctrl+C

### Apache httpd
Let's do the same with an apache webserver:
```bash
docker run -p 80:80 -v $(pwd):/usr/local/apache2/htdocs/ httpd:2.4-alpine
```
And again, the website is accessible on your ip adress.


## Now the same with a php web application
This should be a simple php web application (without composer). If you don't have one, take the following: [WeaselCMS](https://github.com/alterebro/WeaselCMS).

```bash
git clone https://github.com/alterebro/WeaselCMS.git
cd WeaselCMS
docker run -p 80:80 -v $(pwd):/var/www/html/ php:7-apache
```

## Excersice

### Do it yourself
Try to run the following project using the nginx:mainline-alpine image: [Timetable](https://github.com/Simai00/Timetable)

??? success "Solution"
    ```bash
    git clone https://github.com/Simai00/Timetable.git
    cd Timetable/
    docker run -p 80:80 -v $(pwd):/usr/share/nginx/html nginx:mainline-alpine
    ```

[^1]: Bind container ports to the host. Retrieved November 14, 2019, from [https://docs.docker.com/v17.09/engine/userguide/networking/default_network/binding/](https://docs.docker.com/v17.09/engine/userguide/networking/default_network/binding/).
[^2]: Use bind mounts. Retrieved November 14, 2019, from [https://docs.docker.com/storage/bind-mounts/](https://docs.docker.com/storage/bind-mounts/).

