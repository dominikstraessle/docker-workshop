# Introduction

Docker is a tool that provides the ability to develop, package and deploy applications in an isolated environment (containers). A container mostly bundles an application with all the required libraries, binaries and configuration and therefore can be executed without any dependencies except the docker engine. This decouples the software from the underlying system and infrastructure. ([^1][^2][^3])

[^1]: Docker overview. Retrieved November 1, 2019, from [https://docs.docker.com/engine/docker-overview/](https://docs.docker.com/engine/docker-overview/).
[^2]: What is Docker?. Retrieved November 1, 2019, from [https://docker-curriculum.com/#what-is-docker-](https://docker-curriculum.com/#what-is-docker-).
[^3]: Docker (software). Retrieved November 1, 2019, from [https://en.wikipedia.org/wiki/Docker_(software)](https://en.wikipedia.org/wiki/Docker_(software)).
