# Setup Digital Ocean

## Workshop Environment
In order to make sure everything works as excepted during this workshop, you should setup and use the following environment.

## Register at DigitalOcean

!!! info "Free $50 in credit over 30 days"
    If you register with this link, you will get **$50 in credit over 30 days**: [DigitalOcean](https://m.do.co/c/ecabdcce2862)

1. Register here: [DigitalOcean](https://m.do.co/c/ecabdcce2862).
2. You have to add a payment method which is either credit card or PayPal. I recommend credit card, as you have to make an initial transaction if you choose PayPal.
3. On the next page fill in the fields as you like (you will work with this project during the workshop).
4. Your registration is now complete.
5. If you want additional 50$ in credit follow this guide: [Additional 50$ DigitalOcean Credit](#additional-50-digitalocean-credit)

!!! attention
    Choose **credit card** as payment method and you **won't be charged** anything.
    If you choose PayPal, you have to make an initial transaction ($5.00 USD) to validate your PayPal account.

    If you don't have a credit card or paypal, someone can share his digitalocean account with you during the workshop and change the password afterwards.

### Additional 50$ DigitalOcean Credit

1. Create a [https://github.com](https://github.com) account with the email address provided by your school (E.g. example@gibmit.ch) or add this address to your already existing account [Read more](https://help.github.com/en/github/setting-up-and-managing-your-github-user-account/adding-an-email-address-to-your-github-account).
2. Visit [https://education.github.com/benefits](https://education.github.com/benefits) and log in with your github credentials.
3. Check your email for an approval of the GitHub Student Developer Pack.
4. Visit [https://education.github.com/pack/offers](https://education.github.com/pack/offers)
5. Search for `digitalocean` and click on the link to receive your offer code.
6. Copy the offer code.
7. Click on the `DigitalOcean website`-Link
8. Log in.
9. On the DigitalOcean Dashboard navigate to Account > Billing
10. Paste your offer code into the corresponding field.
11. You now have an additonal credit of $50.

### Create Digital Ocean Droplet

!!! note "Shared account during workshop"
    If you share your account during the workshop, everyone has to complete the following steps and should also choose a meaningful hostname for his droplet.

1. Open the DigitalOcean Dashboard
2. Navigate to `Droplets`
3. Click on `Create`
4. Configure the droplet like described below and create it.
5. You will receive an email with the initial root password.

???+ info "Configuration"
    **Image:**  
    Go to the `Marketplace`-Tab, search and select the Docker Image if you want docker to be pre-installed. Else choose Ubuntu 18.04.

    **Plan:**  
    Choose `Standard` and select the $5/mo or $10/mo plan.

    **Region:**  
    Select the nearest. Mine is Frankfurt.

    **Authentication:**  
    Select your SSH-Keys. [How to add SSH-Keys](#generate-ssh-keys)

    **Hostname:**  
    If you share youre digitalocean account with someone else during this workshop, change it to something you can differ. Otherwise you can keep the default values.

    **Project:**  
    Select the project for the workshop.

    Click `Create Droplet` and wait a few seconds till your droplet is ready.

### Open console

1. Open the DigitalOcean Dashboard
2. Navigate to `Droplets`
3. Click on you droplet and open the tab `Access`
4. Click on `Launch Console`
5. A new window appears and you can login with your credentials
6. Now you have a completely setup instance with the configuration described here: [Docker Droplet](https://do.co/2PQDXut)

!!! info "Initial Login"
    When you log into your droplet for the first time, you have to use the following credentials:
    
    - user: root
    - password: password from email

    Afterwards you will be prompted to change the password. If you get an "Invalid login", read the answers from this community question: [Can't login to console](https://www.digitalocean.com/community/questions/login-incorrect-message-after-changing-the-password).

    Also run `#!bash apt update && apt upgrade` when you log in for the first time.

### Connect to digital ocean droplet with SSH

Read the instructions here: [How to Connect to your Droplet with OpenSSH](https://www.digitalocean.com/docs/droplets/how-to/connect-with-ssh/openssh/)

### Initial Ubuntu Setup

Read the instructions here: [Initial Server Setup with Ubuntu 18.04](https://www.digitalocean.com/community/tutorials/initial-server-setup-with-ubuntu-18-04)

### Add Swap Space

Your machine has only 1G of memory, so an additional gig for swap is recommended: [How To Add Swap Space on Ubuntu 18.04](https://www.digitalocean.com/community/tutorials/how-to-add-swap-space-on-ubuntu-18-04)

## Generate SSH Keys

There are several ways to generate SSH Keys.

- Windows: [How to Create SSH Keys with PuTTY on Windows](https://www.digitalocean.com/docs/droplets/how-to/add-ssh-keys/create-with-putty/)
- Linux / macOS / WSL: [ How to Create SSH Keys with OpenSSH on MacOS or Linux](https://www.digitalocean.com/docs/droplets/how-to/add-ssh-keys/create-with-openssh/)

After you generated the keys you have to add your public key to your digital ocean account: [How to Upload SSH Public Keys to a DigitalOcean Account](https://www.digitalocean.com/docs/droplets/how-to/add-ssh-keys/to-account/)
