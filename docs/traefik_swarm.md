# Traefik + Docker Swarm Mode

With traefik and docker in swarm mode, we can deploy applications very easily.

I prepared a template with the following services:

- traefik
    - service discovery
    - auto let's encrypt certificates
- bitwarden

Clone the project from here: [traefik-swarm-setup](https://gitlab.com/dominikstraessle/traefik-swarm-setup).

Replace the EMAIL value inside `traefik/traefik.yml` with your personal email.

Replace the DOMAIN value inside `service/docker-compose.yml` with your domain.

Set the hostname of your server:
```bash
echo DOMAIN > /etc/hostname
hostname -F /etc/hostname
```

Activate docker swarm:
```bash
docker swarm init --advertise-addr IP_ADDRESS
```

Start the traefik stack:
```bash
docker stack deploy -c traefik/docker-compose.yml traefik
```
Deploy the bitwarden service:
```bash
docker stack deploy -c service/docker-compose.yml bitwarden
```

Check the services:
```bash
docker service ls
```

Scale bitwarden to start one replica:
```bash
docker service scale bitwarden_bitwarden=1
```

Visit your DOMAIN!

!!! info "Let's Encrypt"
    It may needs some time to pull a valid certificate, so you may have to wait a moment before you visit your just deployed website.

