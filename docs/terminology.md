# Terminology

## Difference between similar technologies[^1]
!!! info "[Docker](https://www.docker.com/)"
    Docker is the container technology that allows you to containerize your applications.
    Docker is the core of using other technologies.

!!! info "[Docker Compose](https://docs.docker.com/compose/)"
    Docker Compose allows configuring and starting multiple Docker containers.
    Docker Compose is mostly used as a helper when you want to start multiple Docker containers and doesn't want to start each one separately using docker run ....
    Docker Compose is used for starting containers on the same host.
    Docker Compose is used instead of all optional parameters when building and running a single docker container.

!!! info "[Docker Swarm](https://docs.docker.com/engine/swarm/)"
    Docker swarm is for running and connecting containers on multiple hosts.
    Docker swarm is a container cluster management and orchestration tool.
    It manages containers running on multiple hosts and does things like scaling, starting a new container when one crashes, networking containers ...
    Docker swarm is docker in production.
    It is the native docker orchestration tool that is embedded in the Docker Engine.
    The docker swarm file named stack file is very similar to a Docker compose file.

!!! info "[Kubernetes](https://kubernetes.io/)"
    Kubernetes is a container orchestration tool developed by Google.
    Kubernete's goal is very similar to that for Docker swarm.

[^1]: What's the difference between docker compose and kubernetes?. Retrieved November 21, 2019, from [https://stackoverflow.com/a/47537046/7130107](https://stackoverflow.com/a/47537046/7130107).

