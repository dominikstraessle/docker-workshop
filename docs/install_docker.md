# Install Docker on Ubuntu 18.04

## Install Docker

From: Get Docker Engine - Community for Ubuntu[^1]

1. Uninstall old version:  
```bash
sudo apt-get remove docker docker-engine docker.io containerd runc
```

2. Update the apt package index:
```bash
sudo apt-get update
```

3. Install packages to allow apt to use a repository over HTTPS:
```bash
sudo apt-get install \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common
```

4. Add Docker’s official GPG key:
```bash
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
```

5. Add the stable docker repository:
```bash
sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable"
```

6. Update the apt package index:
```bash
sudo apt-get update
```

7. Install the latest version of Docker Engine - Community and containerd:
```bash
sudo apt-get install docker-ce docker-ce-cli containerd.io
```

8. Verify that Docker Engine - Community is installed correctly by running the hello-world image.
```bash
sudo docker run hello-world
```

### Post-installation steps

From: Post-installation steps for Linux[^2]

#### Run docker commands without sudo

1. Create the docker group:
```bash
sudo groupadd docker
```

2. Add your user to the docker group:
```bash
sudo usermod -aG docker $USER
```

3. Close the ssh session, reconnect and verify that you can run docker commands without sudo:
```bash
docker run hello-world
```

#### Configure docker to start on boot

1. Use `systemd` to start docker on boot:
```bash
sudo systemctl enable docker
```


## Install docker-compose

From: Install Docker Compose[^3]

1. Run this command to download the current stable release of Docker Compose:  
```bash
sudo curl -L https://github.com/docker/compose/releases/download/1.25.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
```

2. Apply executable permissions to the binary:
```bash
sudo chmod +x /usr/local/bin/docker-compose
```

[^1]: Get Docker Engine - Community for Ubuntu. Retrieved November 6, 2019, from [https://docs.docker.com/install/linux/docker-ce/ubuntu/](https://docs.docker.com/install/linux/docker-ce/ubuntu/).
[^2]: Post-installation steps for Linux. Retrieved November 6, 2019, from [https://docs.docker.com/install/linux/linux-postinstall/](https://docs.docker.com/install/linux/linux-postinstall/).
[^3]: Install Docker Compose. Retrieved November 21, 2019, from [https://docs.docker.com/compose/install/](https://docs.docker.com/compose/install/).


