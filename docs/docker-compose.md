# docker-compose

With docker-compose you can easily run multi-container applications. It's created for isolated development environments with dependencies on other services, automated test environments, and recently also for production single host deployments. All services can be defined in a single docker-compose.yml file and docker-compose will build and pull the images, creates containers and starts them in the same network.[^1]

Docker compose can also be used to persist all the optional arguments of `docker build` and `docker run`, such as port bindings, volume definitions and build arguments.

## Transformation example

We created a dockerfile for the [vue-hackernews-2.0](https://github.com/vuejs/vue-hackernews-2.0) project. In this chapter we will create a docker-compose.yml file to persist the port bindings and the build context.

Until now we built and started the project with the dockerfile like this:
```bash
docker build -t hacker-news:latest .
docker run -p 80:8080 hacker-news:latest
```

We can transform these two commands into a docker-compose file:
```yaml
version: "3.7"

services:
  # Name of the service
  vue:
    # Build context
    build: .
    # Port bindings
    ports:
      - 80:8080
```

And to build and start the service, we just have to type:
```bash
docker-compose up
```

The app is now available again on port 80.

## Exercise

### Simple docker-compose.yml

Now you have to do such a transformation with the following project, for which you already created a dockerfile: [gibm-timetable](https://github.com/dominikstraessle/gibm-timetable).

Create a docker-compose.yml for this project, so that you can run it with `docker-compose up` afterwards.

??? success "Solution"
    Before we used the following commands for building and running the container:
    ```bash
    docker build -t gibm-timetable:nginx .
    docker run -p 80:80 gibm-timetable:nginx
    ```
    And the resulting docker-compose.yml file looks like this:
    ```yaml
    version: "3.7"

    services:
      timetable:
        build: .
        ports:
          - 80:80
    ```

    And to start the services:
    ```bash
    docker-compose up
    ```

### Stop docker compose services
Find out how to stop the docker compose services you just started.

??? success "Solution"
    ```bash
    docker-compose down
    ```

### Docker compose with volume binding

A popular use case of docker compose is the creation of isolated development environments. If you use docker, you can develop and run your php web application locally without even installing php or a webserver. We already created a docker image of the [WeaselCMS](https://github.com/alterebro/WeaselCMS) php application.

You will now set up a development environment for this application:

- Create a docker-compose.yml file with the following features:
  - port bindings
  - volume bindings, so that local code changes are instantly available inside the container

??? hint "Image, Dockerfile or what else?"
    We haven't created a Dockerfile for this project yet, but you can complete this task without it.
    
    Options:

    - Without Dockerfile: Use the image configuration [Image](https://docs.docker.com/compose/compose-file/#image)
    - or create a Dockerfile for the project and use the build configuration [Build](https://docs.docker.com/compose/compose-file/#build)

!!! info "Start docker-compose services in the background"
    You can detach `docker-compose up` command by using the `-d` option like this:
    ```bash
    docker-compose up -d
    ```

??? success "Solution"
    docker-compose.yml file:
    ```yaml
    version: "3.7"

    services:
      weasel:
        image: php:7-apache
        ports:
          - 80:80
        volumes:
          - ./:/var/www/html/
    ```

    Then run:
    ```bash
    docker-compose up -d
    ```

    !!! hint "Code changes"
        With this solution, all code changes you make on your local device are instantly available in the container as you bound the current working directory with **`./:/var/www/html/`**.

        Just try it out!

        1. Edit the following file:
        ```bash
        vi weasel-cms/data.dat
        ```
        2. At the end of the file change the value from false to true.
        3. Save
        4. Reload the page.

## Docker Compose with other services
If your application has some dependencies on other services, e.g. it requires a database or an api, then you can define these services in the docker-compose file and they will be started and managed by docker-compose.

Let's create a docker-compose file defining adminer and a database.

[Adminer](https://github.com/vrana/adminer): Database management in a single PHP file
[MariaDB](https://github.com/MariaDB/server): MariaDB server is a community developed fork of MySQL server

```yaml
version: '3.7'

services:

  adminer:
    image: adminer
    ports:
      - 80:8080

  db:
    image: mariadb
    environment:
      - MYSQL_ROOT_PASSWORD=example
```

The two services adminer and db are started in the same network.
You can log into the database on the web interface with `root:example`.

### Environment Variables and Database init scripts
Let's take the same two services, but modify them to create a new database on the startup. And in addition let's also create a user with limited privileges on the database.

Create a .env file:
```.env
DB_DATABASE=jmp
DB_USERNAME=test
DB_PASSWORD=test
MYSQL_ROOT_PASSWORD=test
```
Add the following env file definition to the db service and remove the `environment`-lines:
```yaml
    env_file:
      - .env
```

Create a new directory: `init`.
Create the following three files within this directory (Content is hyperlinked):

- [01-createTables.sql](https://raw.githubusercontent.com/jmp-app/jmp/master/docker/db/01_setup.sql)
- [02-createUser.sh](https://raw.githubusercontent.com/jmp-app/jmp/master/docker/db/02_createUser.sh)
- [03-insertData.sql](https://raw.githubusercontent.com/jmp-app/jmp/master/docker/db/03_initData.sql)

Add the following bind mound (volume binding) to the db service:
```yaml
    volumes:
      - ./init/:/docker-entrypoint-initdb.d/
```

Start the containers and wait a moment until the database is ready. Then you can visit the web interface again and login with the credentials from the .env file.

!!! attention "Remove the existing containers before startup"
    You should stop and remove the existing containers before you start the modified one. Just run:
    ```bash
    docker-compose down -v
    ```
    This removes the containers and the named volumes.

### Persistence
The data inside a container only lives as long the container lives. E.g. in case you have a database service, you may want to make the data persistent and available also after a restart. To make this possible we can use [named volumes](https://docs.docker.com/storage/volumes/).

With named volumes you have the following advantages[^2]:

- Volumes are easier to back up or migrate than bind mounts.
- You can manage volumes using Docker CLI commands or the Docker API.
- Volumes work on both Linux and Windows containers.
- Volumes can be more safely shared among multiple containers.
- Volume drivers let you store volumes on remote hosts or cloud providers, to encrypt the contents of volumes, or to add other functionality.
- New volumes can have their content pre-populated by a container.

In our example we use MariaDB. All data files are stored in the following directory on the container: `/var/lib/mysql`. If we want to persist this data, we have to create the following volume definition:
```yaml
volumes:
  dbdata:
    driver: local
```

and add the volume to the service (just append the line after the other volume binding):
```yaml
      - dbdata:/var/lib/mysql/
```

The compose file is ready and the services can be startet. This volume definition makes sure, that all the database data is also available after the lifecycle of the container ends. It can also be use to create a backup or to share the data.

!!! note "Execution of database init scripts"
    The database init scripts are only executed if the database is started for the first time and no data is available at all.

!!! note "Remove data/volumes"
    You may want to delete a volume or all data it contains. 

    Search the name of the volume:
    ```bash
    docker volume ls
    ```

    and remove the volume:
    ```bash
    docker volume rm NAME_OF_THE_VOLUME
    ```

## Complete Example
```yaml
# docker-compose.yml
version: '3.7'

services:

  adminer:
    image: adminer
    ports:
      - 80:8080

  db:
    image: mariadb
    env_file:
      - .env
    volumes:
      - ./init/:/docker-entrypoint-initdb.d/
      - dbdata:/var/lib/mysql/


volumes:
  dbdata:
    driver: local
```

```
#.env
DB_DATABASE=jmp
DB_USERNAME=test
DB_PASSWORD=test
MYSQL_ROOT_PASSWORD=test
```

and of course the init directory with it's three init scripts:

- [01-createTables.sql](https://raw.githubusercontent.com/jmp-app/jmp/master/docker/db/01_setup.sql)
- [02-createUser.sh](https://raw.githubusercontent.com/jmp-app/jmp/master/docker/db/02_createUser.sh)
- [03-insertData.sql](https://raw.githubusercontent.com/jmp-app/jmp/master/docker/db/03_initData.sql)


## Dockerize php project

Example of a dockerized php project without composer or any other dependencies.

Project: [Ikarus_GamblingSite](https://github.com/dominikstraessle/Ikarus_GamblingSite). (This is the Dockerized version of [Ikarus_GamblingSite](https://github.com/BastianBueeler/Ikarus_GamblingSite))

!!! note "home.php"
    You have to visit http://IP_ADDRESS/home.php to access the application.

!!! attention "Initialize database"
    Unless you use a framework like Laravel or Symfony, you have to initialize the database more or less manually before you start the application. This can also be done with the docker images of a database. Most databases support init scripts like we learned in the previous section.

!!! attention "Environment Variables"
    You never should hardcode configuration values like the database host, password, etc...
    For this purpose there are environment variables. Just put the config in an `.env` file as key-value pairs and read them from the `$_ENV` array at the runtime.

    You never should have the `.env` file checked into your vcs. Add this files to your `.gitignore` file. Nevertheless, in this example the `.env` file is checked into the git history.

!!! attention "Database host"
    When you develop with a XAMPP or LAMPP setup, the host of the database is `localhost`. But if you work with docker-compose, the host of the database is the name of the database service declared in the docker-compose file.

!!! attention "PHP extensions"
    By default, your XAMPP/LAMPP setup has all common php extensions installed by default. This is not the case when you use the php docker images.
    To install additional php extensions such as `mysqli`, `pdo`, `json`, etc..., you have to install them like this in the dockerfile:
    ```Dockerfile
    RUN docker-php-ext-install mysqli
    ```

!!! attention "Connect to the database from host"
    By default the db service is only available from inside the docker network. If you want to access the database during development, E.g. with DataGrip or PHPStorm, you have to expose the port 3306 to the host. You can do this with the following lines inside the docker compose service:
    ```yaml
    ports:
      - 3306:3306
    ```

## Exercises

### Add adminer service to the IkarusGamblingSite project

Add the adminer service from the previous example to the IkarusGamblingSite, so that you can manage your database over the web interface.

The service should be available over the port `8080`.

??? success "Solution"
    Just add the following service to the docker-compose.yml file:
    ```yaml
    adminer:
      image: adminer
      ports:
        - 8080:8080
    ```

### Dockerize a project as you wish

Choose any project and try to dockerize it.

!!! attention "Limited memory on VPS"
    The virtual private servers, which we're using during this workshop has limited memory. So an application which requires more than 1 GB of memory may not be able to run. That also applies for builds requiring much memory. To name some: Java Web Applications and NodeJS builds can be very memory consuming.

[^1]: Overview of Docker Compose. Retrieved November 21, 2019, from [https://docs.docker.com/compose/](https://docs.docker.com/compose/).
[^2]: Use volumes. Retrieved November 28, 2019, from [https://docs.docker.com/storage/volumes/](https://docs.docker.com/storage/volumes/).
