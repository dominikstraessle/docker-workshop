# Dockerfiles
In the last section, you've started web servers hosting a simple website. This is only possible because we don't have to compile or build anything. But as soon as your web application needs additional dependencies during the runtime or if it's written in a compiled language like Java, you have to build the project before serving.

This is possible with [Dockerfiles](https://docs.docker.com/engine/reference/builder/). You can define all build instructions, the configuration and the entrypoint of the container in the Dockerfile.

We will work us through some examples with plain html/css/js, Vue.js and JSF.

## Dockerfile without build instructions

Here's a simple example how a Dockerfile for the gibm-timetable would look like:
```Dockerfile
FROM nginx:mainline-alpine
COPY . /usr/share/nginx/html
```

To build the image run:
```bash
docker build -t gibm-timetable:nginx .
```

Check the image:
```bash
docker image ls
```

and to run the built image:
```bash
docker run -p 80:80 gibm-timetable:nginx
```

Again, the website is accessible on your ip adress.

## Exercises
### Dockerfile with build instructions (Node/Vue.js)
The goal of this exercise is to learn the basics of the Dockerfile syntax and use cases.

You are given the following vue project: [vue-hackernews-2.0](https://github.com/vuejs/vue-hackernews-2.0).

1. Clone the project
2. Write a Dockerfile which builds the project and runs a web server
3. Build the docker image based on the Dockerfile and tag it with `hacker-news:latest`
4. Run a container of the image and bind the ports, so that you can access the web application on our IP adress on port 80


??? hint "Relevant Vue.js build commands"
    You have to use the following three commands in the right order to build and serve the project (in addition to the copy commands):

    1. `npm install`
    2. `npm run build`
    3. `npm start`
    
    They are also listed here: [Build Setup](https://github.com/vuejs/vue-hackernews-2.0#build-setup)

    Remember: Use your google skills.

??? success "Solution"
    
    Clone:
    ```bash
    git clone https://github.com/vuejs/vue-hackernews-2.0.git
    cd vue-hackernews-2.0
    ```
    Create Dockerfile:

    ```Dockerfile
    FROM node:lts-alpine
    # Set working directory
    WORKDIR /app
    # Copy only dependencies list
    COPY package*.json ./
    # Install dependencies
    RUN npm install
    # Copy the rest of the project
    COPY . .
    # Build the project
    RUN npm run build
    # The command to start the webserver
    EXPOSE 8080
    CMD ["npm", "start"]
    ```

    Build:
    ```bash
    docker build -t hacker-news:latest .
    ```

    Run:
    ```bash
    docker run -p 80:8080 hacker-news:latest
    ```

## Dockerfile with mutli stage build (Java/Maven/Vaadin/Tomcat)

Now we want to create a Dockerfile for a vaadin application with a more complex build.

You are given a vaadin sample project and you have to dockerize it with a multi stage build.

Project: [Vaadin UI Examples](https://github.com/vaadin/ui-examples)

Enter the following directory (We will use one of all these example projects): 
```bash
cd ui-examples/data-entry/customer-crud/customer-crud-flow
```

In this case it's the best option to use mutli stage builds. A multi stage build uses different docker base images for building and running the application. With our application it's the maven base image for building and the tomcat base image for serving.

A normal build flow would look like this in general:

1. Pull build base image
2. Install build dependencies
3. Copy source to build
4. Build
5. Pull runtime base image
6. Install runtime dependencies
7. Copy build artifact from build stage
8. Define entrypoint and ports

In this example it looks like this:

Pull build base image and name it (in addition set the WORKDIR):
```Dockerfile
FROM maven:3.6.0-jdk-8-slim AS build
WORKDIR /app
```

Install build dependencies is executed after copying the source in this example, as we install nodejs with a maven command. If we would install nodejs the common way, it would be done here.

Copy source to build:
```Dockerfile
COPY . .
```

Build (and install nodejs):


!!! note "Install nodejs before running `mvn package`"
    As this project depends on nodejs to build it's frontend, you have to install it before running `mvn package`.
    Install it like this: `RUN mvn com.github.eirslett:frontend-maven-plugin:1.7.6:install-node-and-npm -DnodeVersion="v12.13.0"`

```Dockerfile
RUN mvn com.github.eirslett:frontend-maven-plugin:1.7.6:install-node-and-npm -DnodeVersion="v12.13.0"
RUN mvn clean package -Pproduction
```

Pull runtime base image:
```Dockerfile
FROM tomcat:9-jdk8-openjdk-slim
```

There are no additional runtime dependencies.

Copy build artifact from build stage:

!!! note "Tomcat deployment directory"
    If you want to deploy a war file to a tomcat webserver, you have to copy the war archive into the following directory:
    `$CATALINA_HOME\webapps`

```Dockerfile
COPY --from=build /app/target/customer-crud-1.0-SNAPSHOT.war $CATALINA_HOME/webapps
```

Entrypoints and ports don't have to be defined, as the ones defined in the tomcat base image are already what we need.

??? success "Complete Dockerfile"
    ```Dockerfile
    FROM maven:3.6.0-jdk-8-slim AS build
    WORKDIR /app
    COPY . .
    RUN mvn com.github.eirslett:frontend-maven-plugin:1.7.6:install-node-and-npm -DnodeVersion="v12.13.0"
    RUN mvn clean package
    FROM tomcat:9-jdk8-openjdk-slim
    COPY --from=build /app/target/customer-crud-1.0-SNAPSHOT.war $CATALINA_HOME/webapps
    ```

Now you can build the docker image:
```bash
docker build -t vaadin:latest .
```

The build will take about 9 minutes.

Then you can run the container:
```bash
docker run -p 80:8080 vaadin:latest
```

And as soon as the war is deployed, the app is accessible on http://IP_ADRESS/customer-crud-1.0-SNAPSHOT/.

!!! note "Dockerize Maven Project"
    A very useful stackoverflow answer: [How to dockerize maven project?](https://stackoverflow.com/a/27768965/7130107).
    You should create a web archive (war) with maven and then serve this with tomcat.

    Here are the relevant docker images:

    - [Maven](https://hub.docker.com/_/maven)
    - [Tomcat](https://hub.docker.com/_/tomcat)

    For this project you have to use java 8.
